import '@/assets/css/styles.css';
import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';
import VueRouter from 'vue-router';
import Router from './router';
import store from './store/index';

Vue.config.productionTip = false;

Vue.use({ VueRouter, Vuex });

const router = Router;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');
