import axios from 'axios';

export default axios.create({
  baseURL: 'http://52.67.112.161/api',
  headers: {
    'Content-type': 'application/json'
  }
});
