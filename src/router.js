import Vue from 'vue';
import Router from 'vue-router';
import FormRegister from './components/FormRegister';
import FormLogin from './components/FormLogin';
import Auth from './views/Auth';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'auth',
      redirect: 'login',
      component: Auth,
      children: [
        {
          path: 'login',
          name: 'login',
          component: FormLogin
        },
        {
          path: 'register',
          name: 'register',
          component: FormRegister
        }
      ]
    }
  ]
});
