import http from '../../http-common';

// initial state
const state = {
  user: null,
  token: null
};

// getters
const getters = {};

// actions
const actions = {
  async logIn(email, password) {
    try {
      const res = await http.post('/login', {
        correo: email,
        password
      });

      console.log(res);
    } catch (error) {
      console.log(error);
    }
  }
};

// mutations
const mutations = {
  logIn(user, token) {
    console.log(user, token);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
